from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoItem, TodoList
from django.http import HttpResponse
from todos.forms import NewList, NewItem

# Create your views here.


def todo_list_list(request):
    todo_list = TodoList.objects.all()

    context = {
        "todo_list": todo_list,
    }

    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    todo = get_object_or_404(TodoList, id=id)
    context = {
        "todo": todo,
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = NewList(request.POST)
        if form.is_valid():
            new_list = form.save()
            return redirect("todo_list_detail", id=new_list.id)
    else:
        form = NewList
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)


def todo_item_create(request):
    if request.method == "POST":
        form = NewItem(request.POST)
        if form.is_valid():
            new_item = form.save()
            return redirect("todo_list_detail", id=new_item.list.id)
    else:
        form = NewItem
    context = {
        "form": form,
    }
    return render(request, "todos/create_item.html", context)


def todo_list_update(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = NewList(request.POST, instance=todo_list)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = NewList(instance=todo_list)
    context = {
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = NewItem(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = NewItem(instance=item)
    context = {
        "form": form,
    }
    return render(request, "todos/edit_item.html", context)


def todo_list_delete(request, id):
    todo_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")
